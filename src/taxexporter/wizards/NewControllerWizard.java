package taxexporter.wizards;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import taxexporter.ui.pages.NewControllerWizardPage;
import taxexporter.util.Consts;
import taxexporter.util.Util;

/**
 * 新建Controller向导
 * 
 * @author sunny
 *
 */
public class NewControllerWizard extends Wizard implements INewWizard {
	private NewControllerWizardPage page;
	private ISelection selection;
	private String projectPath, containerName, url;

	/**
	 * Constructor for NewControllerWizard.
	 */
	public NewControllerWizard() {
		super();
		setWindowTitle("New Tax Controller");
		setNeedsProgressMonitor(true);
	}

	/**
	 * Adding the page to the wizard.
	 */

	public void addPages() {
		page = new NewControllerWizardPage(selection);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We
	 * will create an operation and run it using wizard as execution context.
	 */
	public boolean performFinish() {
		containerName = page.getContainerName();
		url = page.getUrl();
		final String fileName = page.getFileName();
		projectPath = Util.getCurrentProject().getLocation().toFile().getAbsolutePath();
		final String finalContainerName = containerName;
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(finalContainerName, fileName, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * The worker method. It will find the container, create the file if missing
	 * or just replace its contents, and open the editor on the newly created
	 * file.
	 */

	private void doFinish(String containerName, String fileName, IProgressMonitor monitor) throws CoreException {
		// create a sample file
		monitor.beginTask("Creating " + fileName, 2);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(containerName));
		if (!resource.exists() || !(resource instanceof IContainer)) {
			throwCoreException("Container \"" + containerName + "\" does not exist.");
		}
		IContainer container = (IContainer) resource;
		final IFile file = container.getFile(new Path(fileName));
		try {
			InputStream stream = openContentStream(fileName);
			if (stream != null) {
				if (file.exists()) {
					file.setContents(stream, true, true, monitor);
				} else {
					file.create(stream, true, monitor);
				}
				stream.close();
			} else {
				monitor.worked(1);
				getShell().getDisplay().asyncExec(new Runnable() {
					public void run() {
						MessageDialog.openError(getShell(), "Error", "Controller file is not exists");
					}
				});
				return;
			}
		} catch (IOException e) {
		}
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IDE.openEditor(page, file, true);
				} catch (PartInitException e) {
				}
			}
		});
		monitor.worked(1);
	}

	/**
	 * We will initialize file contents with a sample text.
	 */

	private InputStream openContentStream(String fileName) {
		String newPackage = "package ";
		Pattern pat = Pattern.compile("src/main/java/(.*)", Pattern.DOTALL);
		Matcher mat = pat.matcher(containerName);
		if (mat.find()) {
			newPackage = newPackage + mat.group(1).replaceAll("/", ".");
		} else {
			return null;
		}
		String contents;
		try {
			contents = Util.file2Str(projectPath + "/plugins/templates/NewController.java", Consts.DEFAULT_ENCODE);
			contents = contents.replaceAll("##url##", url).replaceAll("##dir##", url)
					.replace("package uf.audit.tax.cz.web.controller", newPackage);
			contents = contents.replace("class NewController ",
					"class " + fileName.substring(0, fileName.length() - 5) + " ");
			return new ByteArrayInputStream(contents.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ByteArrayInputStream("Error".getBytes());
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status = new Status(IStatus.ERROR, "TaxIDE", IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}