package taxexporter.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import taxexporter.wizards.NewControllerWizard;

/**
 * 新建Controller命令
 * 
 * @author sunny
 *
 */
public class NewController extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell parentShell = HandlerUtil.getActiveShell(event);
		IWizard pg = new NewControllerWizard();
		WizardDialog dlg = new WizardDialog(parentShell, pg);
		dlg.open();
		return null;
	}

}
