package taxexporter.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import taxexporter.util.Util;
import taxexportor.views.WebComponentView;

/**
 * open WebControl
 * 
 * @author sunny
 *
 */
public class OpenWebControl extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Util.openView(WebComponentView.ID);
		return null;
	}

}
