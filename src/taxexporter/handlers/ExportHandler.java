package taxexporter.handlers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import taxexporter.util.Util;

/**
 * 导出项目，生成war包
 * 
 * @author sunny
 * 
 */

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ExportHandler extends AbstractHandler {
	private static final int TOTAL_STEP = 180;
	private String projectPath;

	/**
	 * The constructor.
	 */
	public ExportHandler() {
	}

	public static final void shellOut(Process p, IProgressMonitor m) throws IOException {
		BufferedInputStream in = null;
		BufferedReader br = null;
		try {
			in = new BufferedInputStream(p.getInputStream());
			br = new BufferedReader(new InputStreamReader(in));
			String s;
			int i = 1;
			while ((s = br.readLine()) != null) {
				System.out.println("\n");
				int pos = s.indexOf("] ");
				if (pos > 0) {
					s = s.substring(pos + 2);
				}
				i++;
				if (!s.startsWith("-------")) {
					m.setTaskName(s.trim());
					m.internalWorked((double) i / 10);
				}
			}
		} catch (IOException e) {
			throw e;
		} finally {
			br.close();
			in.close();
		}
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IProject project = Util.getCurrentProject();
		final String projectPath = (project).getLocation().makeAbsolute().toFile().getAbsolutePath();
		final Shell shell = window.getShell();
		Job buildJob = new Job("开始构建项目") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				// try {
				// monitor.beginTask("开始构建", TOTAL_STEP);
				// File executeFile = null;
				// ProcessBuilder pb = null;
				// if (System.getenv("OS").toLowerCase().contains("linux")) {
				// executeFile = new File(projectPath + File.separator +
				// "package.sh");
				// String[] command = { "/bin/sh", executeFile.getAbsolutePath()
				// };
				// pb = new ProcessBuilder(command);
				// } else {
				// executeFile = new File(projectPath + File.separator +
				// "package.bat");
				// String[] command = { "cmd", "/c",
				// executeFile.getAbsolutePath() };
				// pb = new ProcessBuilder(command);
				// }
				// final File packageFile = executeFile;
				// Process p;
				// try {
				// p = pb.start();
				// shellOut(p, monitor);
				// } catch (IOException e) {
				// e.printStackTrace();
				// }
				shell.getDisplay().asyncExec(new Runnable() {

					@Override
					public void run() {
						MessageDialog.openInformation(shell, "提示",
								"请使用 mvn clean package -Dmaven.test.skip=true 命令构建系统！");
					}
				});

				// String[] explorerCommand = { "explorer", "/SELECT,",
				// packageFile.getParentFile().getAbsolutePath() +
				// "\\target\\tax.war" };
				// Runtime.getRuntime().exec(explorerCommand);
				return Status.OK_STATUS;
				// } catch (IllegalArgumentException e) {
				// e.printStackTrace();
				// } catch (SecurityException e) {
				// e.printStackTrace();
				// } catch (IOException e) {
				// e.printStackTrace();
				// } finally {
				// monitor.done();
				// }
				// return Status.CANCEL_STATUS;
			}
		};

		buildJob.setUser(true);
		buildJob.schedule();
		return null;
	}
}
