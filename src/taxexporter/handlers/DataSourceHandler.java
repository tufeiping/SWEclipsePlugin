package taxexporter.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import taxexporter.ui.DataSourceDialog;
import taxexporter.util.Util;

/**
 * DataSource 命令
 * 
 * @author sunny
 * 
 */

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class DataSourceHandler extends AbstractHandler {

	/**
	 * The constructor.
	 */
	public DataSourceHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IProject project = Util.getCurrentProject();
		String projectPath = (project).getLocation().makeAbsolute().toFile().getAbsolutePath();
		DataSourceDialog dialog = new DataSourceDialog(window.getShell(), projectPath);
		dialog.open();
		return null;
	}
}
