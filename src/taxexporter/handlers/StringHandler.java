package taxexporter.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import taxexporter.util.Util;
import taxexporter.views.StringToolsView;

/**
 * 字符串工具
 * 
 * @author sunny
 *
 */
public class StringHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Util.openView(StringToolsView.ID);
		return null;
	}

}
