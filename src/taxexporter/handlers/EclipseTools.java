package taxexporter.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

/**
 * 工具，获取eclipse的各个view的id，开发时需要
 * 
 * @author sunny
 *
 */
public class EclipseTools extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPage wbp = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		String id = wbp.getActivePartReference().getId();
		MessageDialog.openInformation(wbp.getActivePart().getSite().getShell(), "ID", String.format("ID is: %s", id));
		return null;
	}

}
