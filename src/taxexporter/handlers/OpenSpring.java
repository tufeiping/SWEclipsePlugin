package taxexporter.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import taxexporter.util.Util;

public class OpenSpring extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Util.openSource("/src/main/resources/spring.xml");
		return null;
	}

}
