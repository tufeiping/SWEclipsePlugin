package taxexporter.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import taxexporter.util.Util;

/**
 * open source modules.js
 * 
 * @author sunny
 *
 */
public class OpenModules extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Util.openSource("/src/main/webapp/js/app/modules.js");
		return null;
	}

}
