package taxexporter.tester;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Path;

/**
 * Popup tester
 * 
 * @author sunny
 *
 */
public class PopupMenuTester extends PropertyTester {

	public PopupMenuTester() {

	}

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		boolean enabled = false;
		if (receiver instanceof IProject) {
			IProject project = (IProject) receiver;
			if (project != null) {
				Path plugins = new Path("plugins");
				IFolder specialFile = project.getFolder(plugins);
				enabled = specialFile.exists();
			}
		}
		return enabled;
	}

}
