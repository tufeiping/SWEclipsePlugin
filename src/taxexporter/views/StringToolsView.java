package taxexporter.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

/**
 * 字符串工具
 * 
 * @author sunny
 *
 */

public class StringToolsView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "taxexporter.views.StringToolsView";

	public StringToolsView() {
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		FormData data;
		parent.setLayout(new FormLayout());
		data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(100, 0);
		data.right = new FormAttachment(100, 0);
		data.top = new FormAttachment(0, 0);
		parent.setLayoutData(data);

		parent.setLayout(new FormLayout());

		white = new Color(parent.getDisplay(), 255, 255, 255); // Display.getCurrent().getSystemColor(SWT.COLOR_WHITE);
		black = new Color(parent.getDisplay(), 0, 0, 0); // Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);

		okButton = new Button(parent, SWT.PUSH);
		okButton.setText("OK");
		data = new FormData();
		data.bottom = new FormAttachment(100, -5);
		data.right = new FormAttachment(100, -5);
		okButton.setLayoutData(data);
		okButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				String demo = text.getText().trim();
				if (demo.equals(""))
					return;

				if (lowcaseButton.getSelection()) {
					destText.setText(demo.toLowerCase());
				} else if (upperButton.getSelection()) {
					destText.setText(demo.toUpperCase());
				} else if (strButton.getSelection()) {
					demo = demo.replaceAll("\"", "\\\\\"");
					demo = demo.replaceAll("\n", "\" +\n \"");
					demo = demo.replaceAll("\r", "");
					demo = "\"" + demo + "\"";
					destText.setText(demo);
				}
				pageTab.setSelection(1);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {

			}
		});

		pageTab = new TabFolder(parent, SWT.BORDER | SWT.TOP);
		data = new FormData();
		data.left = new FormAttachment(0, 5);
		data.right = new FormAttachment(100, -5);
		data.top = new FormAttachment(0, 5);
		data.bottom = new FormAttachment(okButton, -5);
		pageTab.setLayoutData(data);

		TabItem pg1 = new TabItem(pageTab, SWT.NONE);
		pg1.setText("SOURCE");
		Composite pg1C = new Composite(pageTab, SWT.NONE);
		pg1C.setLayout(new FormLayout());
		data = new FormData();
		data.left = new FormAttachment(0, 5);
		data.right = new FormAttachment(100, -5);
		data.top = new FormAttachment(0, 5);
		data.bottom = new FormAttachment(100, -5);
		pg1C.setLayoutData(data);
		pg1.setControl(pg1C);

		text = new Text(pg1C, SWT.MULTI);
		// text.setBackground(black);
		data = new FormData();
		data.top = new FormAttachment(0, 5);
		data.left = new FormAttachment(0, 5);
		data.right = new FormAttachment(100, -5);
		data.bottom = new FormAttachment(100, -5);
		text.setLayoutData(data);
		// text.setForeground(white);
		text.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent arg0) {
				white.dispose();
				black.dispose();
			}
		});

		TabItem pg2 = new TabItem(pageTab, SWT.NONE);
		pg2.setText("TARGET");
		Composite pg2C = new Composite(pageTab, SWT.NONE);
		pg2C.setLayout(new FormLayout());
		data = new FormData();
		data.left = new FormAttachment(0, 5);
		data.right = new FormAttachment(100, -5);
		data.top = new FormAttachment(0, 5);
		data.bottom = new FormAttachment(100, -5);
		pg2C.setLayoutData(data);
		pg2.setControl(pg2C);

		destText = new Text(pg2C, SWT.MULTI);
		// destText.setBackground(black);
		data = new FormData();
		data.top = new FormAttachment(0, 5);
		data.left = new FormAttachment(0, 5);
		data.right = new FormAttachment(100, -5);
		data.bottom = new FormAttachment(100, -5);
		destText.setLayoutData(data);
		// destText.setForeground(white);

		strButton = new Button(parent, SWT.RADIO);
		strButton.setText("TO_STRING");
		data = new FormData();
		data.right = new FormAttachment(okButton, -5);
		data.bottom = new FormAttachment(100, -5);
		strButton.setLayoutData(data);

		upperButton = new Button(parent, SWT.RADIO);
		upperButton.setText("UPPER");
		data = new FormData();
		data.right = new FormAttachment(strButton, -5);
		data.bottom = new FormAttachment(100, -5);
		upperButton.setLayoutData(data);

		lowcaseButton = new Button(parent, SWT.RADIO);
		lowcaseButton.setText("LOWER");
		data = new FormData();
		data.right = new FormAttachment(upperButton, -5);
		data.bottom = new FormAttachment(100, -5);
		lowcaseButton.setLayoutData(data);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		text.setFocus();
	}

	private Text text, destText;
	private TabFolder pageTab;
	private Color white, black;
	private Button okButton, lowcaseButton, upperButton, strButton;
}