package taxexporter.ui.pages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import taxexporter.util.Util;
import taxexporter.wizards.NewPageWizard;

/**
 * 新建页面
 * 
 * @author sunny
 *
 */

public class NewPagePage extends WizardPage {
	private ISelection selection;
	private NewPageWizard caller;
	private Text pageUrlText, pagePathText, pageControllerText, pageDescText;
	private List<Button> buttons = new ArrayList<Button>();

	public NewPagePage(ISelection selection) {
		super("wizard");
		setTitle("New page");
		setDescription("New page,including JS and Page");
		setPageComplete(false);
		this.selection = selection;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		container.setLayout(layout);

		GridData layoutData = new GridData();
		layoutData.widthHint = 300;
		layoutData.heightHint = 22;

		Label pageDesc = new Label(container, SWT.NULL);
		pageDesc.setText("Page Instruction");
		pageDescText = new Text(container, SWT.BORDER);
		pageDescText.setText("");
		pageDescText.setLayoutData(layoutData);

		Label pageController = new Label(container, SWT.NULL);
		pageController.setText("controller");
		pageControllerText = new Text(container, SWT.BORDER);
		pageControllerText.setText("");
		pageControllerText.setLayoutData(layoutData);
		pageControllerText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent event) {
				String text = pageControllerText.getText().trim();
				caller.isOk = !text.equals("");
				setPageComplete(caller.isOk);
			}
		});

		Label pageUrl = new Label(container, SWT.NULL);
		pageUrl.setText("URL");
		pageUrlText = new Text(container, SWT.BORDER);
		pageUrlText.setText("/url/method");
		pageUrlText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent event) {
				String path = pageUrlText.getText();
				pagePathText.setText(path);
			}
		});
		pageUrlText.setLayoutData(layoutData);

		Label pagePath = new Label(container, SWT.NULL);
		pagePath.setText("Page Url");
		pagePathText = new Text(container, SWT.BORDER);
		pagePathText.setText("/url/method");
		pagePathText.setLayoutData(layoutData);

		Label pageType = new Label(container, SWT.NULL);
		pageType.setText("Page Type");

		Composite pageTypeContainer = new Composite(container, SWT.NULL);
		GridLayout pgTypeLayout = new GridLayout();
		pgTypeLayout.numColumns = 4;
		pageTypeContainer.setLayout(pgTypeLayout);

		IFolder tmpls = Util.getCurrentProject().getFolder("/plugins/templates");
		try {
			IResource[] res = tmpls.members();
			for (IResource re : res) {
				String name = re.getName();
				if (name.endsWith(".html")) {
					name = name.substring(0, name.length() - 5);
					Button pgTypes = new Button(pageTypeContainer, SWT.RADIO);
					pgTypes.setText(name);
					buttons.add(pgTypes);
				}
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		setControl(container);
	}

	public void setCaller(NewPageWizard caller) {
		this.caller = caller;
	}

	public Text getPageUrlText() {
		return pageUrlText;
	}

	public Text getPagePathText() {
		return pagePathText;
	}

	public Text getPageControllerText() {
		return pageControllerText;
	}

	public List<Button> getButtons() {
		return buttons;
	}

	public Text getPageDescText() {
		return pageDescText;
	}

}
