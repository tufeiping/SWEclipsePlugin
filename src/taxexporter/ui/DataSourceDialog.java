package taxexporter.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * 数据源 Dialog
 * 
 * @author sunny
 *
 */

public class DataSourceDialog extends Dialog {
	private String projectPath, dbcfg;

	Text ip_text, username_text, password_text;
	Shell shell;

	String ip, username, password;

	public DataSourceDialog(Shell parentShell, String path) {
		super(parentShell);
		shell = parentShell;
		projectPath = path;
		dbcfg = projectPath + "/src/main/resources/db.properties";
	}

	private void saveInput() {
		ip = ip_text.getText();
		username = username_text.getText();
		password = password_text.getText();
	}

	public DataSourceDialog(IShellProvider parentShell) {
		super(parentShell);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(400, 250);
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("设置数据源");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());
		container.setSize(500, 260);

		FormData data;

		Label ip = new Label(container, SWT.FLAT);
		ip.setText("JDBC串");
		data = new FormData();
		data.left = new FormAttachment(0, 10);
		data.top = new FormAttachment(0, 10);
		ip.setLayoutData(data);

		Label username = new Label(container, SWT.FLAT);
		username.setText("Username");
		data = new FormData();
		data.left = new FormAttachment(0, 10);
		data.top = new FormAttachment(ip, 20);
		username.setLayoutData(data);

		Label password = new Label(container, SWT.FLAT);
		password.setText("Password");
		data = new FormData();
		data.left = new FormAttachment(0, 10);
		data.top = new FormAttachment(username, 20);
		password.setLayoutData(data);

		Properties p = new Properties();
		FileInputStream inStream;
		try {
			inStream = new FileInputStream(dbcfg);
			p.load(inStream);
			inStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		ip_text = new Text(container, SWT.LEFT);
		ip_text.setText(p.getProperty("jdbcUrl", ""));
		data = new FormData();
		data.left = new FormAttachment(30, 0);
		data.top = new FormAttachment(0, 10);
		data.right = new FormAttachment(100, -10);
		ip_text.setLayoutData(data);

		username_text = new Text(container, SWT.LEFT);
		username_text.setText(p.getProperty("user", ""));
		data = new FormData();
		data.left = new FormAttachment(30, 0);
		data.top = new FormAttachment(ip_text, 20);
		data.right = new FormAttachment(100, -10);
		username_text.setLayoutData(data);

		password_text = new Text(container, SWT.LEFT);
		password_text.setText(p.getProperty("password", ""));
		data = new FormData();
		data.left = new FormAttachment(30, 0);
		data.top = new FormAttachment(username_text, 20);
		data.right = new FormAttachment(100, -10);
		password_text.setLayoutData(data);

		return container;
	}

	@Override
	protected void okPressed() {
		saveInput();
		super.okPressed();
		boolean isOk = true;
		StringBuilder sb = new StringBuilder();
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(
					dbcfg)));
			String line;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line.startsWith("#")) { // comment
					sb.append(line).append("\n");
				} else {
					if (line.startsWith("jdbcUrl")) {
						sb.append("jdbcUrl=" + ip).append("\n");
					} else if (line.startsWith("user")) {
						sb.append("user=" + username).append("\n");
					} else if (line.startsWith("password")) {
						sb.append("password=" + password).append("\n");
					} else {
						sb.append(line).append("\n");
					}
				}
			}
			br.close();
			FileWriter fw = new FileWriter(new File(dbcfg));
			fw.write(sb.toString());
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			isOk = false;
		} catch (IOException e) {
			e.printStackTrace();
			isOk = false;
		}

		if (isOk)
			MessageDialog.openInformation(shell, "成功ʾ", "数据源设置成功");
		else
			MessageDialog.openError(shell, "失败", "数据源设置失败");
	}
}
